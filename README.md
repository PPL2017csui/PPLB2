Cermatrack
==========

[![build status](https://gitlab.com/PPL2017csui/PPLB2/badges/140611483_start_trip/build.svg)](https://gitlab.com/PPL2017csui/PPLB2/commits/140611483_start_trip)
[![coverage report](https://gitlab.com/PPL2017csui/PPLB2/badges/140611483_start_trip/coverage.svg)](https://gitlab.com/PPL2017csui/PPLB2/commits/140611483_start_trip)

Sistem ini merupakan mobile app yang membantu kurir Cermati dalam melakukan pekerjaannya untuk
antar/jemput dokumen client (nasabah). Sistem ini membantu kurir dengan pemberian task secara
optimal, memberi navigasi pada kurir, mempermudah proses persiapan dokumen, serta memungkinkan
admin untuk memonitor lokasi kurir secara semi real-time.

Team
----

Anjayyy Dev™

Repository
----------

https://gitlab.com/PPL2017csui/PPLB2