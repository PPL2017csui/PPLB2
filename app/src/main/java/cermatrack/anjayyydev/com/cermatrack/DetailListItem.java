package cermatrack.anjayyydev.com.cermatrack;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import cermatrack.anjayyydev.com.cermatrack.Model.DocumentRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Presenter.ListDocumentPresenter;

public class DetailListItem extends AppCompatActivity implements OnMapReadyCallback, DirectionCallback, ListDocumentPresenter.ListDocumentPresenterListener {
    private static final String TASK_ID = "taskid";
    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String REGION = "region";
    private static final String DESC = "desc";
//    private static final String TASK_STATUS = "taskstatus";
    private static final String DEADLINE = "deadline";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
//    private static final String COURIER = "courier";
//    private static final String ASSIGN_BY = "assignby";
//    private static final String ALARM = "alarm";
//    private static final String CATEGORY = "category";

    Toolbar mToolbar;
    TextView region, address, name, description, deadline;
    LinearLayout documents;
    ArrayList<DocumentRecord> listDocuments;
    int taskId;
    String address_eta, latitude, longitude;
    private GoogleMap googleMap;
    private LatLng client_location;
    String travel_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_list_item);

//        get Intent and Information in Intent
        Intent i = getIntent();
        taskId = i.getIntExtra(TASK_ID, 0);
        latitude = i.getStringExtra(LATITUDE);
        longitude = i.getStringExtra(LONGITUDE);
        address_eta = i.getStringExtra(ADDRESS);
//        create task
        listDocumentReady();
        listDocuments = (ArrayList<DocumentRecord>) ListDocumentPresenter.getDocuments().documents;

//        define layout
        address = (TextView) findViewById(R.id.detail_address);
        region = (TextView) findViewById(R.id.detail_region);
        name = (TextView) findViewById(R.id.detail_name);
        description = (TextView) findViewById(R.id.detail_description);
        deadline = (TextView) findViewById(R.id.detail_deadline);
        mToolbar = (Toolbar) findViewById(R.id.detail_act_toolbar);
        documents = (LinearLayout) findViewById(R.id.list_detail_documents);

//        set documents
        for (DocumentRecord doc : listDocuments) {
            View view = getLayoutInflater().inflate(R.layout.document_list_view, null);
            TextView documentName = (TextView) view.findViewById(R.id.detail_document);
            TextView documentDescription = (TextView) view.findViewById(R.id.detail_document_desc);

//            set info
            documentName.setText(doc.name);
            documentDescription.setText(doc.description);
            documents.addView(view);
        }

//        set value
        region.setText(i.getStringExtra(REGION));
        name.setText(i.getStringExtra(NAME));
        description.setText(i.getStringExtra(DESC));
        deadline.setText(i.getStringExtra(DEADLINE));

//        back button on toolbar for back to MainActivity savedstate
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        gmap things
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        // Add a marker in client's location
        // and move the map's camera to the same location.
        client_location = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

        try {
            googleMap.setMyLocationEnabled(true);
            LocationManager lm = (LocationManager) getSystemService(this.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());

            String key = getApplicationContext().getString(R.string.google_maps_key);
            GoogleDirection.withServerKey(key)
                    .from(origin)
                    .to(client_location)
                    .execute(this);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Silakan aktifkan GPS pada perangkat Anda terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    void drawMarker(String duration) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(client_location)
                .zoom(15)
                .bearing(0)
                .tilt(45)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.addMarker(new MarkerOptions()
                .position(client_location)
                .title("Lokasi Klien\n" + travel_time)
        );

        address.setText(address_eta + "\n" + "(" + travel_time + ")");
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        // Do something here
        if (direction.isOK()) {
            travel_time = direction.getRouteList().get(0).getLegList().get(0).getDuration().getText();
            drawMarker(travel_time);
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        // Do something here
    }

    @Override
    public void listDocumentReady() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListDocumentPresenter listDocument = new ListDocumentPresenter();
        listDocument.createDocuments(taskId);
    }
}
