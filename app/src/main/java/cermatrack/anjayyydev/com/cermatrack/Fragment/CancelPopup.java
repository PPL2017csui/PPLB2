package cermatrack.anjayyydev.com.cermatrack.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cermatrack.anjayyydev.com.cermatrack.MainActivity;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Presenter.ListTaskPresenter;
import cermatrack.anjayyydev.com.cermatrack.Presenter.TripPresenter;
import cermatrack.anjayyydev.com.cermatrack.R;

/**
 * Created by falah on 19/04/2017.
 */

public class CancelPopup extends Activity{
    private Button sendReasonBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cancel_popup);
        DisplayMetrics dm = new DisplayMetrics();
        // beware of changing the default display
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.85), (int)(height*.3));

        sendReasonBtn = (Button) findViewById(R.id.send_reason_button);
        sendReasonBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                EditText nameField = (EditText) findViewById(R.id.cancel_reason);
                String reason = nameField.getText().toString();

                if (reason.equals("")) {
                    Toast.makeText(getApplicationContext(), "Alasan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else {
                    TaskRecord currentTask = ListTaskPresenter.getCurrentTask();
                    TripPresenter.cancel(currentTask, reason);
                    Log.d("cancel", "task cancelled because : " + reason);
                    finish();

                    Intent homeIntent = new Intent(CancelPopup.this, MainActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);
                }
            }
        });
    }
}
