package cermatrack.anjayyydev.com.cermatrack.Fragment;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import cermatrack.anjayyydev.com.cermatrack.MainActivity;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Presenter.ListTaskPresenter;
import cermatrack.anjayyydev.com.cermatrack.Presenter.TripPresenter;
import cermatrack.anjayyydev.com.cermatrack.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements DirectionCallback, OnMapReadyCallback, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TEXT = "arg_text";
    private Button cancelBtn;
    private Button mButtonGo;

    // TODO: Rename and change types of parameters
    private String mText;

    private Toolbar mToolbar;

    MapView mMapView;
    private GoogleMap googleMap;
    public LatLng origin;
    public LatLng destination;

    protected TaskRecord currentTask;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param text Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String text) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // retrieve text and color from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mText = args.getString(ARG_TEXT);
        } else {
            mText = savedInstanceState.getString(ARG_TEXT);
        }

        currentTask = ListTaskPresenter.getCurrentTask();

        // initialize views
        mToolbar = (Toolbar) view.findViewById(R.id.home_toolbar);
        cancelBtn = (Button) view.findViewById(R.id.cancel_button);
        mButtonGo = (Button) view.findViewById(R.id.button_go);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent intentPop = new Intent(getActivity().getApplicationContext(), CancelPopup.class);
                startActivity(intentPop);
            }
        });
        TextView detail = (TextView) view.findViewById(R.id.detail);
        final TextView textView = (TextView) view.findViewById(R.id.name);

        mButtonGo.setOnClickListener(this);
        if (currentTask == null) {
            cancelBtn.setVisibility(View.INVISIBLE);
            mButtonGo.setVisibility(View.INVISIBLE);

            textView.setText(Html.fromHtml("<b>&lt; Kosong &gt;</b>"));
            String detailText = "<h4>&lt; Kosong &gt;<h4>"
                    + "<p><i>Sudah tidak ada tugas untuk hari ini.</i></p>";
            detail.setText(Html.fromHtml(detailText));
        } else {

            if(! currentTask.task_status.equals("not done")) {
                mButtonGo.setVisibility(View.INVISIBLE);
                cancelBtn.setVisibility(View.INVISIBLE);
            }

            textView.setText(Html.fromHtml("<b>" + currentTask.name + "</b><br>" + currentTask.address));
            String detailText = "<h4>"+currentTask.name+"<h4>"
                    + "<p><i>Alamat</i><br>"+currentTask.address+" - "+currentTask.region+"</p>"
                    + "<p><i>Waktu</i><br>"+currentTask.deadline+"</p>"
                    + "<p><i>Deskripsi</i><br>"+currentTask.description+"</p>";
            detail.setText(Html.fromHtml(detailText));
        }

        SlidingUpPanelLayout slidingUpPanelLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset > 0.5) {
                    textView.setText(Html.fromHtml("<b>Detail Tugas</b>"));
                    textView.setTextSize(22);

                } else {
                    if (currentTask != null) {
                        textView.setText(Html.fromHtml("<b>" + currentTask.name + "</b><br>" + currentTask.address));
                    } else {
                        textView.setText(Html.fromHtml("<b>&lt; Kosong &gt;</b>"));
                    }
                    textView.setTextSize(14);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

            }
        });

        ((MainActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(mText);
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {
            Route route = direction.getRouteList().get(0);
            Leg leg = route.getLegList().get(0);
            ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
            PolylineOptions polylineOptions = DirectionConverter.createPolyline(this.getContext(), directionPositionList, 5, Color.RED);

            googleMap.addPolyline(polylineOptions);
            Marker a = googleMap.addMarker(new MarkerOptions().position(origin));
            Marker b = googleMap.addMarker(new MarkerOptions().position(destination));

            List<Marker> markers = new ArrayList<>();
            markers.add(a);
            markers.add(b);

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            int padding = (int) (getResources().getDisplayMetrics().widthPixels * 0.10); // offset from edges of the map 10% of screen
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.animateCamera(cu);
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        try {
            googleMap = mMap;
            googleMap.setMyLocationEnabled(true);

            LocationManager lm = (LocationManager) getActivity().getSystemService(getContext().LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            origin = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder().target(origin).zoom(10).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            if (currentTask != null && currentTask.task_status.equals("ongoing")) {
                mButtonGo.performClick();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity().getApplicationContext(), "Silakan aktifkan GPS pada perangkat Anda terlebih dahulu", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        mButtonGo.setVisibility(View.INVISIBLE);
        cancelBtn.setVisibility(View.INVISIBLE);

        if (this.origin != null) {
            TripPresenter mTripPresenter = new TripPresenter();

            if (currentTask != null) {
                mTripPresenter.start(currentTask);
                ApplicationInfo applicationInfo = null;
                try {
                    applicationInfo = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), PackageManager.GET_META_DATA);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                Bundle bundle = applicationInfo.metaData;

                destination  = new LatLng(Double.parseDouble(currentTask.latitude), Double.parseDouble(currentTask.longitude));
                GoogleDirection.withServerKey(bundle.getString("com.google.android.geo.API_KEY"))
                        .from(origin)
                        .to(destination)
                        .transportMode(TransportMode.DRIVING)
                        .execute(this);
            }
        } else {
            Toast.makeText(getActivity().getApplicationContext(), "Silakan aktifkan GPS pada perangkat Anda terlebih dahulu", Toast.LENGTH_LONG).show();
        }
    }
}
