package cermatrack.anjayyydev.com.cermatrack.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cermatrack.anjayyydev.com.cermatrack.Fragment.tasks.FailedTask;
import cermatrack.anjayyydev.com.cermatrack.Fragment.tasks.NextTask;
import cermatrack.anjayyydev.com.cermatrack.Fragment.tasks.SuccessfulTask;
import cermatrack.anjayyydev.com.cermatrack.MainActivity;
import cermatrack.anjayyydev.com.cermatrack.R;
import cermatrack.anjayyydev.com.cermatrack.adapters.TaskFragmentAdapter;

public class TaskFragment extends Fragment {
    private static final String ARG_TEXT = "arg_text";

    private String mText;

    private ViewPager mViewPager;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;

    public TaskFragment() {
        // Biarkan kosong
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param text Parameter 1.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(String text) {
        Fragment fragment = new TaskFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // retrieve text and color from bundle or savedInstanceState
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            mText = args.getString(ARG_TEXT);
        } else {
            mText = savedInstanceState.getString(ARG_TEXT);
        }

        mToolbar = (Toolbar) view.findViewById(R.id.tasks_toolbar);
        ((MainActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(mText);

        mViewPager = (ViewPager) view.findViewById(R.id.fragment_task);
        setUpViewPager(mViewPager);

        mTabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task, container, false);
    }

    private void setUpViewPager(ViewPager mViewPager) {
        TaskFragmentAdapter mAdapter = new TaskFragmentAdapter(getChildFragmentManager());
        mAdapter.addFragment(NextTask.newInstance(getString(R.string.tab_next_task)), getResources().getString(R.string.tab_next_task));
        mAdapter.addFragment(SuccessfulTask.newInstance(getString(R.string.tab_successful_task)), getResources().getString(R.string.tab_successful_task));
        mAdapter.addFragment(FailedTask.newInstance(getString(R.string.tab_failed_task)), getResources().getString(R.string.tab_failed_task));
        mViewPager.setAdapter(mAdapter);
    }
}
