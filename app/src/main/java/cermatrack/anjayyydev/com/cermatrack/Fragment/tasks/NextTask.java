package cermatrack.anjayyydev.com.cermatrack.Fragment.tasks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cermatrack.anjayyydev.com.cermatrack.DetailListItem;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Presenter.ListTaskPresenter;
import cermatrack.anjayyydev.com.cermatrack.R;
import cermatrack.anjayyydev.com.cermatrack.adapters.DividerItemDecoration;
import cermatrack.anjayyydev.com.cermatrack.adapters.NextTaskViewAdapter;
import cermatrack.anjayyydev.com.cermatrack.adapters.RecyclerViewClickSupport;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NextTask#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NextTask extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TEXT = "arg_text";
    private static final String TASK_ID = "taskid";
    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String REGION = "region";
    private static final String DESC = "desc";
    private static final String TASK_STATUS = "taskstatus";
    private static final String DEADLINE = "deadline";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String COURIER = "courier";
    private static final String ASSIGN_BY = "assignby";
    private static final String ALARM = "alarm";
    private static final String CATEGORY = "category";

    // TODO: Rename and change types of parameters
    private ArrayList<TaskRecord> mTasksRecordList = new ArrayList<>();
    private RecyclerView mRecycleView;
    private NextTaskViewAdapter mAdapter;

    public NextTask() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param text Parameter 1.
     * @return A new instance of fragment NextTask.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(String text) {
        NextTask fragment = new NextTask();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecycleView = (RecyclerView) getActivity().findViewById(R.id.next_task_view);
        mTasksRecordList = (ArrayList<TaskRecord>) getTaskRecordList();
        mAdapter = new NextTaskViewAdapter(mTasksRecordList);

        RecyclerView.LayoutManager mLayoutManage = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecycleView.setLayoutManager(mLayoutManage);
        mRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRecycleView.addItemDecoration(new DividerItemDecoration(this.getContext(), LinearLayoutManager.VERTICAL));
//        set the adapter of lists
        mRecycleView.setAdapter(mAdapter);

//        item on touch
        RecyclerViewClickSupport.addTo(mRecycleView).setOnItemClickListener(new RecyclerViewClickSupport.OnItemClickListener() {

            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), DetailListItem.class);
                TaskRecord selectedTask = mTasksRecordList.get(position);
                i.putExtra(TASK_ID, selectedTask.id);
                i.putExtra(NAME, selectedTask.name);
                i.putExtra(ADDRESS, selectedTask.address);
                i.putExtra(REGION, selectedTask.region);
                i.putExtra(DESC, selectedTask.description);
                i.putExtra(TASK_STATUS, selectedTask.task_status);
                i.putExtra(DEADLINE, selectedTask.deadline);
                i.putExtra(LATITUDE, selectedTask.latitude);
                i.putExtra(LONGITUDE, selectedTask.longitude);
                i.putExtra(COURIER, selectedTask.courier_id);
                i.putExtra(ASSIGN_BY, selectedTask.deadline);
                i.putExtra(ALARM, selectedTask.isSetAlarm);
                i.putExtra(CATEGORY, selectedTask.category);

                startActivity(i);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next_task, container, false);
    }

    public List<TaskRecord> getTaskRecordList() {
        ArrayList<TaskRecord> list = new ArrayList<>();
        for (TaskRecord task : ListTaskPresenter.getTasks().tasks) {
            if (task.task_status.equals("not done")) {
                list.add(task);
            }
        }
        return list;
    }

}
