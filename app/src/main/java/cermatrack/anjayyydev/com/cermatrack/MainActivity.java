package cermatrack.anjayyydev.com.cermatrack;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;

import cermatrack.anjayyydev.com.cermatrack.Fragment.HomeFragment;
import cermatrack.anjayyydev.com.cermatrack.Fragment.SettingsFragment;
import cermatrack.anjayyydev.com.cermatrack.Fragment.TaskFragment;
import cermatrack.anjayyydev.com.cermatrack.Presenter.ListTaskPresenter;
import cermatrack.anjayyydev.com.cermatrack.Presenter.TaskReminderPresenter;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements ListTaskPresenter.ListTaskPresenterListener, TaskReminderPresenter.TaskReminderPresenterListener {

    private static final String SELECTED_ITEM = "arg_selected_item";

    private BottomNavigationView mBottomNavigation;
    private int mSelectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        mBottomNavigation = (BottomNavigationView) findViewById(R.id.navigation);
        mBottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectMenu(item);
                return true;
            }
        });

        MenuItem mMenuItem;
        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            mMenuItem = mBottomNavigation.getMenu().findItem(mSelectedItem);
        }
        else {
            mMenuItem = mBottomNavigation.getMenu().getItem(0);
        }
        selectMenu(mMenuItem);

        // Create instance of ListTaskPresenter
        listTaskReady();

        // Create instance of TaskReminder
        taskReminderReady();
    }

    @Override
    public void listTaskReady() {
        if (ListTaskPresenter.getTasks() == null) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            ListTaskPresenter listTask = new ListTaskPresenter();
            listTask.createTasks();
        }
    }

    @Override
    public void taskReminderReady() {
        TaskReminderPresenter taskReminder = new TaskReminderPresenter(this);
        taskReminder.createNotif();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        MenuItem homeItem = mBottomNavigation.getMenu().getItem(0);
        if (mSelectedItem != homeItem.getItemId()) {
            // select home item
            selectMenu(homeItem);
        } else {
            super.onBackPressed();
        }
    }

    private void selectMenu(MenuItem mItem) {
        Fragment fragment = null;

        switch (mItem.getItemId()) {
            case R.id.navigation_home:
                fragment = HomeFragment.newInstance(getString(R.string.title_home));
                break;
            case R.id.navigation_tasks:
                fragment = TaskFragment.newInstance(getString(R.string.title_tasks));
                break;
            case R.id.navigation_settings:
                fragment = SettingsFragment.newInstance(getString(R.string.title_settings));
                break;
        }

        mSelectedItem = mItem.getItemId();
        mItem.setChecked(true);

        if (fragment != null) {
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.replace(R.id.container, fragment);
            beginTransaction.commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
