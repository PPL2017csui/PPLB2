package cermatrack.anjayyydev.com.cermatrack.Model;


/**
 * Created by nandanaprabaswara on 3/22/17.
 */

public class DocumentRecord extends BaseRecord {

    public int id;

    public String name;

    public int task_id;

    public int quantity;

    public String description;

    public DocumentRecord(int id, String name, int task_id, int quantity, String description) {
        this();
        this.id = id;
        this.name = name;
        this.task_id = task_id;
        this.quantity = quantity;
        this.description = description;
    }

    public DocumentRecord() {
        super();
    }

    @Override
    public String toString() {
        return "DocumentRecord{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", task_id=" + task_id +
                ", quantity=" + quantity +
                ", description=" + description +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(((DocumentRecord) o).toString());
    }
}
