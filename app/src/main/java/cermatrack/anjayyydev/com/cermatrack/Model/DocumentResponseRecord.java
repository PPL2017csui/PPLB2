package cermatrack.anjayyydev.com.cermatrack.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fatah on 4/21/2017.
 */

public class DocumentResponseRecord extends BaseRecord {
    @SerializedName("documents")
    public List<DocumentRecord> documents;

    public DocumentResponseRecord() {
        super();
        documents = new ArrayList<>();
    }

    public DocumentResponseRecord(List<DocumentRecord> documents) {
        this();
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "DocumentResponseRecord{" +
                "documents=" + documents +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(((DocumentResponseRecord) o).toString());
    }
}
