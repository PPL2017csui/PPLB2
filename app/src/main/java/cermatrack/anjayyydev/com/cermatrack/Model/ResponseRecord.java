package cermatrack.anjayyydev.com.cermatrack.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muhammadgilangjanuar on 4/17/17.
 */

public class ResponseRecord extends BaseRecord {
    @SerializedName("status")
    String status;

    public ResponseRecord() {
        super();
    }

    public ResponseRecord(String status) {
        this();
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResponseRecord{" +
                "status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(((ResponseRecord) o).toString());
    }
}
