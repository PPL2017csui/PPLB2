package cermatrack.anjayyydev.com.cermatrack.Model;

/**
 * Created by muhammadgilangjanuar on 3/19/17.
 */

public class TaskRecord extends BaseRecord {

    public int id;

    public String name;

    public String description;

    public String address;

    public String region;

    public String deadline;

    public String task_status;

    public String latitude;

    public String longitude;

    public String courier_id;

    public String assigned_by;

    public boolean isSetAlarm;

    public int category;

    public TaskRecord() {
        super();
    }

    public TaskRecord(int id, String name, String description, String address, String region, String deadline, String task_status, String latitude, String longitude, String courier_id, String assigned_by, int category) {
        this();
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.region = region;
        this.deadline = deadline;
        this.task_status = task_status;
        this.latitude = latitude;
        this.longitude = longitude;
        this.courier_id = courier_id;
        this.assigned_by = assigned_by;
        this.isSetAlarm = false;
        this.category = category;
    }

    @Override
    public String toString() {
        return "TaskRecord{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", region='" + region + '\'' +
                ", deadline='" + deadline + '\'' +
                ", task_status='" + task_status + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", courier_id='" + courier_id + '\'' +
                ", assigned_by='" + assigned_by + '\'' +
                ", isSetAlarm=" + isSetAlarm +
                ", category=" + category +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(((TaskRecord) o).toString());
    }
}
