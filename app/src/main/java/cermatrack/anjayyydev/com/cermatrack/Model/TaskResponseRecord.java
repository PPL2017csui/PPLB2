package cermatrack.anjayyydev.com.cermatrack.Model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by falahprasetyo edited by muhammadgilangjanuar on 3/20/17.
 */

public class TaskResponseRecord extends ResponseRecord {

    @SerializedName("tasks")
    public List<TaskRecord> tasks;

    public TaskResponseRecord() {
        super();
        tasks = new ArrayList<>();
    }

    public TaskResponseRecord(List<TaskRecord> tasks) {
        this();
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "TaskResponseRecord{" +
                "tasks=" + tasks +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(((TaskResponseRecord) o).toString());
    }
}
