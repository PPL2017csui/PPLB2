package cermatrack.anjayyydev.com.cermatrack.Presenter;

import java.io.IOException;

import cermatrack.anjayyydev.com.cermatrack.Model.DocumentResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Service.DocumentService;

/**
 * Created by Fatah on 4/21/2017.
 */

public class ListDocumentPresenter extends BasePresenter {
    private final DocumentService documentService;
    public static DocumentResponseRecord documentResponseRecord;

    public interface ListDocumentPresenterListener {
        void listDocumentReady();
    }

    public ListDocumentPresenter() {
        this.documentService = new DocumentService();
    }

    public DocumentService getDocumentService() {
        return this.documentService;
    }

    public static DocumentResponseRecord getDocuments() {
        return documentResponseRecord;
    }

    public void createDocuments(int taskId) {
        try {
            documentResponseRecord = documentService.getAPI().getResult(taskId).execute().body();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
