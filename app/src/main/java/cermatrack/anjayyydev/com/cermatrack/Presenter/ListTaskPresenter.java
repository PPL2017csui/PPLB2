package cermatrack.anjayyydev.com.cermatrack.Presenter;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Service.TaskService;

/**
 * Created by falahprasetyo edited by muhammadgilangjanuar on 3/20/17.
 */

public class ListTaskPresenter extends BasePresenter {

    private final TaskService taskService;
    public static TaskResponseRecord taskResponses;

    public interface ListTaskPresenterListener {
        void listTaskReady();
    }

    public ListTaskPresenter(){
        this.taskService = new TaskService();
    }

    public static TaskResponseRecord getTasks() {
        return taskResponses;
    }

    // still not perfect
    public static TaskRecord getCurrentTask() {
        List<TaskRecord> tasks = taskResponses.tasks;
        TaskRecord currentTask = null;
        int i = 0;

        while (i < tasks.size() && ! tasks.get(i).task_status.equals("ongoing") && ! tasks.get(i).task_status.equals("not done")) {
            i++;
        }

        if (i < tasks.size()) {
            currentTask = tasks.get(i);
        }

        return currentTask;
    }

    public void createTasks(){
        try {
            taskResponses = taskService.getAPI().getResults().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
