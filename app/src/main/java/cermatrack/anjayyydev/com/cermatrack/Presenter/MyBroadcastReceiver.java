package cermatrack.anjayyydev.com.cermatrack.Presenter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import cermatrack.anjayyydev.com.cermatrack.MainActivity;
import cermatrack.anjayyydev.com.cermatrack.R;

/**
 * Created by hamdan.fachry on 3/28/2017.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        addNotification(context, intent);
    }

    private void addNotification(Context context, Intent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentText(intent.getExtras().getString("deadline"));
        builder.setContentTitle(intent.getExtras().getString("name") + " - Cermatrack Reminder");
        builder.setAutoCancel(true);
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }


}
