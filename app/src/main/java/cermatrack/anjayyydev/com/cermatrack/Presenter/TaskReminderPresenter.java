package cermatrack.anjayyydev.com.cermatrack.Presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Service.TaskService;
import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.concurrency.Task;
import retrofit2.Callback;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by hamdan.fachry on 4/7/2017.
 */

public class TaskReminderPresenter extends BasePresenter {

    private final Context context;
    TaskResponseRecord taskResponses;

    public interface TaskReminderPresenterListener {
        void taskReminderReady();
    }

    public TaskReminderPresenter(Context context){
        this.context = context;
    }

    public void createNotif() {
        taskResponses = ListTaskPresenter.getTasks();
        addAlarm();
    }

    private void addAlarm() {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        int id = 0;
        for(TaskRecord i : taskResponses.tasks) {
            try {
                Intent intent = new Intent(context, MyBroadcastReceiver.class);
                intent.putExtra("deadline", i.deadline);
                intent.putExtra("name", i.name);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context, id++, intent, 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = sdf.parse(i.deadline);
                long millis = date.getTime();
                int tenMinutes = 600000;
                if(millis - tenMinutes > System.currentTimeMillis()) {
                    //set timer 10 minutes before deadline
                    alarmManager.set(AlarmManager.RTC_WAKEUP, millis - tenMinutes,
                            pendingIntent);
                }
            }
            catch (ParseException e) {
                Log.d("parse exception", e.getMessage());
            }
        }
    }

}
