package cermatrack.anjayyydev.com.cermatrack.Presenter;

import android.util.Log;

import java.io.IOException;

import cermatrack.anjayyydev.com.cermatrack.Model.ResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Service.CancelTripService;
import cermatrack.anjayyydev.com.cermatrack.Service.StartTripService;
import cermatrack.anjayyydev.com.cermatrack.Service.TaskService;

/**
 * Created by muhammadgilangjanuar on 4/13/17.
 */

public class TripPresenter extends BasePresenter {
    public static ResponseRecord response;

    public TripPresenter() {
        super();
    }

    public void start(TaskRecord taskRecord) {
        try {
            StartTripService startService = new StartTripService();
//             dummy, harusnya ngambil this courrier id
            ResponseRecord goResponse = startService.getAPI().giveName(taskRecord.id + "", taskRecord.courier_id)
                    .execute().body();
            response = goResponse;
//             do what you want to do with the response
            Log.d("go status", "status : " + goResponse.toString());

            TaskRecord currentTask = ListTaskPresenter.getCurrentTask();
            currentTask.task_status = "ongoing";

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException np) {
            np.printStackTrace();
            Log.d("go status", "status : 404 response not found");
        }
    }

    public static void cancel(TaskRecord taskRecord, String reason) {
        try {
            CancelTripService cancelService = new CancelTripService();
            // dummy, harusnya ngambil this courrier id
            ResponseRecord cancelResponse = cancelService.getAPI().giveReason(taskRecord.id + "", taskRecord.courier_id, reason).execute().body();
            response = cancelResponse;
            // do what you want to do with the response
            Log.d("cancel status", "status : " + cancelResponse.toString());

            taskRecord.task_status = "failed";

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException np) {
            np.printStackTrace();
            Log.d("cancel status", "status : 404 response not found");
        }
    }
}
