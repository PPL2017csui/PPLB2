package cermatrack.anjayyydev.com.cermatrack.Service;

/**
 * Created by falah on 18/03/2017.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import cermatrack.anjayyydev.com.cermatrack.Model.ResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskResponseRecord;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CermatrackFirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * The Application's current Instance ID token is no longer valid and thus a new one must be requested.
     */
    @Override
    public void onTokenRefresh() {
        String tokenId = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "FCM Token: " + tokenId);

        try {
            sendTokenToDB4(tokenId);
        } catch (Exception e) {
            Log.d("ada", "exception");
        }
    }

    private void sendTokenToDB4(String tokenId) throws IOException {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://redbeard.mgilangjanuar.com/misc/notification-test/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TokenAPI api =retrofit.create(TokenAPI.class);

        Log.d("jalanin","request");
        Call<ResponseRecord> call = api.giveToken(tokenId);
        call.execute();
        Log.d("jalanin","berhasil");
    }
}
