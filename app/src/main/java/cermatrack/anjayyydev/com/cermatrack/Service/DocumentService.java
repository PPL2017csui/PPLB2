package cermatrack.anjayyydev.com.cermatrack.Service;

import cermatrack.anjayyydev.com.cermatrack.Model.DocumentResponseRecord;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Fatah on 4/21/2017.
 */

public class DocumentService {
    private static String BASE_URL = "https://private-a9d5c-trip10.apiary-mock.com/";


    public interface DocumentAPI {
        @GET("documents/{taskId}")
        Call<DocumentResponseRecord> getResult(@Path("taskId") int taskId);
    }

    public DocumentAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(DocumentAPI.class);

    }
}
