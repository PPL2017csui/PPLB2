package cermatrack.anjayyydev.com.cermatrack.Service;

import cermatrack.anjayyydev.com.cermatrack.Model.ResponseRecord;
import retrofit2.Retrofit;
import retrofit2.Call;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by muhammadgilangjanuar on 4/13/17.
 */

public class StartTripService {
    private static String BASE_URL =  "https://private-a9d5c-trip10.apiary-mock.com/trip/";

    public interface StartAPI{
        @FormUrlEncoded
        @POST("start")
        Call<ResponseRecord> giveName(
                @Field("task_id") String task_id, @Field("courier_id") String courier_id);
    }

    public StartAPI getAPI(){
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(StartAPI.class);
    }
}
