package cermatrack.anjayyydev.com.cermatrack.Service;

import cermatrack.anjayyydev.com.cermatrack.Model.ResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskResponseRecord;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by falah on 28/03/2017.
 */

public interface TokenAPI {
    @GET("save.php")
    Call<ResponseRecord> giveToken(
            @Query("token") String token);
}
