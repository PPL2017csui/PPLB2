package cermatrack.anjayyydev.com.cermatrack.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cermatrack.anjayyydev.com.cermatrack.R;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;

/**
 * Created by Fatah on 3/28/2017.
 */

public class NextTaskViewAdapter extends RecyclerView.Adapter<NextTaskViewAdapter.NextTaskViewHolder> {
    private ArrayList<TaskRecord> mTasksRec;

    public class NextTaskViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, deadline;

        public NextTaskViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.nx_name);
            desc = (TextView) view.findViewById(R.id.nx_desc);
            deadline = (TextView) view.findViewById(R.id.nx_deadline);
        }
    }

    public NextTaskViewAdapter(ArrayList<TaskRecord> tasksRec) {
        mTasksRec = tasksRec;
    }

    @Override
    public NextTaskViewAdapter.NextTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.next_task_view, parent, false);
        return new NextTaskViewAdapter.NextTaskViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(NextTaskViewAdapter.NextTaskViewHolder holder, int position) {
        TaskRecord tasks = mTasksRec.get(position);
        holder.title.setText(tasks.name);
        holder.desc.setText(tasks.description);
        holder.deadline.setText(tasks.deadline + "");
    }

    @Override
    public int getItemCount() {
        return mTasksRec.size();
    }
}
