package cermatrack.anjayyydev.com.cermatrack.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cermatrack.anjayyydev.com.cermatrack.R;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;

/**
 * Created by Fatah on 3/28/2017.
 */

public class SuccessfulTaskViewAdapter extends RecyclerView.Adapter<SuccessfulTaskViewAdapter.SuccessfulTaskViewHolder> {
    private ArrayList<TaskRecord> mTasksRec;

    public class SuccessfulTaskViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, deadline;

        public SuccessfulTaskViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.suc_name);
            desc = (TextView) view.findViewById(R.id.suc_desc);
            deadline = (TextView) view.findViewById(R.id.suc_deadline);
        }
    }

    public SuccessfulTaskViewAdapter(ArrayList<TaskRecord> tasksRec) {
        mTasksRec = tasksRec;
    }

    @Override
    public SuccessfulTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.successful_task_view, parent, false);
        return new SuccessfulTaskViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(SuccessfulTaskViewHolder holder, int position) {
        TaskRecord tasks = mTasksRec.get(position);
        holder.title.setText(tasks.name);
        holder.desc.setText(tasks.description);
        holder.deadline.setText(tasks.deadline + "");
    }

    @Override
    public int getItemCount() {
        return mTasksRec.size();
    }
}
