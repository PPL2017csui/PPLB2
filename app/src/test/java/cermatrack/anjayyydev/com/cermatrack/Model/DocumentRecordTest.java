package cermatrack.anjayyydev.com.cermatrack.Model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by muhammadgilangjanuar on 4/5/17.
 */
public class DocumentRecordTest {

    @Test
    public void create() {
        int id = 1;
        String name = "Hello";
        int task_id = 154;
        int quantity = 10;
        String description = "Hi";

        DocumentRecord actual = new DocumentRecord(id, name, task_id, quantity, description);
        String expected = "DocumentRecord{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", task_id=" + task_id +
                ", quantity=" + quantity +
                ", description=" + description +
                '}';

        assertEquals(expected, actual.toString());

        int temp = 2;
        DocumentRecord wrongActual = new DocumentRecord(temp, name, task_id, quantity, description);
        assertNotEquals(expected, wrongActual.toString());

        DocumentRecord actual1 = new DocumentRecord(id, name, task_id, quantity, description);
        assertTrue(actual.equals(actual1));
    }
}