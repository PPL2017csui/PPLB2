package cermatrack.anjayyydev.com.cermatrack.Model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by muhammadgilangjanuar on 4/5/17.
 */
public class TaskRecordTest {


    @Test
    public void create() {
        int id = 1;
        String name = "Budi";
        String description = "Bank BCA";
        String address = "Depok";
        String region = "Jabar";
        String deadline = "08-07-2014 22:00:00";
        String task_status = "done";
        String latitude = "1234567";
        String longitude = "1224.123";
        String courier_id = "123";
        String assigned_by = "Lutfy";
        boolean isSetAlarm = false;
        int category = 1;

        TaskRecord actual = new TaskRecord(id, name, description, address, region, deadline, task_status, latitude, longitude, courier_id, assigned_by, category);
        String expected = "TaskRecord{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", region='" + region + '\'' +
                ", deadline='" + deadline + '\'' +
                ", task_status='" + task_status + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", courier_id='" + courier_id + '\'' +
                ", assigned_by='" + assigned_by + '\'' +
                ", isSetAlarm=" + isSetAlarm +
                ", category=" + category +
                '}';
        assertEquals(expected, actual.toString());

        int temp = 2;
        TaskRecord wrongActual = new TaskRecord(temp, name, description, address, region, deadline, task_status, latitude, longitude, courier_id, assigned_by, category);
        assertNotEquals(expected, wrongActual.toString());

        TaskRecord actual1 = new TaskRecord(id, name, description, address, region, deadline, task_status, latitude, longitude, courier_id, assigned_by, category);
        assertTrue(actual.equals(actual1));
    }
}