package cermatrack.anjayyydev.com.cermatrack.Model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by muhammadgilangjanuar on 4/5/17.
 */
public class TaskResponseRecordTest {

    @Test
    public void create() {
        List<TaskRecord> tasks = new ArrayList<>();

        int id = 1;
        String name = "Budi";
        String description = "Bank BCA";
        String address = "Depok";
        String region = "Jabar";
        String deadline = "08-07-2014 22:00:00";
        String task_status = "done";
        String latitude = "1234567";
        String longitude = "1224.123";
        String courier_id = "123";
        String assigned_by = "Lutfy";
        boolean isSetAlarm = false;
        int category = 1;
        TaskRecord task1 = new TaskRecord(id, name, description, address, region, deadline, task_status, latitude, longitude, courier_id, assigned_by, category);

        tasks.add(task1);

        TaskResponseRecord actual = new TaskResponseRecord(tasks);
        String expected = "TaskResponseRecord{" +
                "tasks=" + tasks +
                '}';
        assertEquals(expected, actual.toString());

        TaskResponseRecord wrongActual = new TaskResponseRecord();
        assertNotEquals(expected, wrongActual.toString());
    }

}