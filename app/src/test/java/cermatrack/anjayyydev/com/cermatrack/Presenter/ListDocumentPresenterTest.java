package cermatrack.anjayyydev.com.cermatrack.Presenter;

import cermatrack.anjayyydev.com.cermatrack.Model.DocumentResponseRecord;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Prabaswara on 4/22/2017.
 */

public class ListDocumentPresenterTest {

    ListDocumentPresenter listDocumentPresenter;
    DocumentResponseRecord expectedDocumentResponseRecord;
    static final String MOCK_JSON = "{\n" +
        "\"status\": 200,\n" +
        "\"documents\": [\n" +
        "{\n" +
        "\"id\": \"111\",\n" +
        "\"name\": \"Dokumen Kredit\",\n" +
        "\"task_id\": \"584\",\n" +
        "\"quantity\": \"1\",\n" +
        "\"description\": \"dokumen aplikasi kredit Bank ABC\"\n" +
        "},\n" +
        "{\n" +
        "\"id\": \"112\",\n" +
        "\"name\": \"Dokumen Kredit 2\",\n" +
        "\"task_id\": \"584\",\n" +
        "\"quantity\": \"1\",\n" +
        "\"description\": \"dokumen aplikasi kredit bank ABC\"\n" +
        "}\n" +
        "]\n}\n\n";

    @Before
    public void prepare() {
        listDocumentPresenter = new ListDocumentPresenter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        expectedDocumentResponseRecord = gson.fromJson(MOCK_JSON, DocumentResponseRecord.class);
    }

    @Test
    public void createDocuments() {
        listDocumentPresenter.createDocuments(584);
        DocumentResponseRecord actualDocumentResponseRecord = listDocumentPresenter.getDocuments();

        assertEquals(expectedDocumentResponseRecord, actualDocumentResponseRecord);

        actualDocumentResponseRecord.documents.remove(0);
        assertNotEquals(expectedDocumentResponseRecord, actualDocumentResponseRecord);
    }
}
