package cermatrack.anjayyydev.com.cermatrack.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskResponseRecord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by muhammadgilangjanuar on 4/7/17.
 */
public class ListTaskPresenterTest {

    ListTaskPresenter listTaskPresenter;
    TaskResponseRecord expectedTaskResponseRecord;
    static final String MOCK_JSON = "{\n" +
            "\"status\": 200,\n" +
            "\"tasks\": [{\n" +
            "\"id\": \"584\",\n" +
            "\"name\": \"Muhammad Budi\",\n" +
            "\"description\": \"Mengantar dokumen aplikasi kredit Bank ABC\",\n" +
            "\"address\": \"Taman Mini Indonesia Indah\",\n" +
            "\"region\": \"Jakarta Timur\",\n" +
            "\"deadline\": \"2017-04-22 08:00:00\",\n" +
            "\"task_status\": \"failed\",\n" +
            "\"latitude\": \"-6.302594\",\n" +
            "\"longitude\": \"106.895218\",\n" +
            "\"courier_id\": \"666\",\n" +
            "\"assigned_by\": \"Lutfi Wiguna\",\n" +
            "\"category\": \"1\"\n" +
            "}, {\n" +
            "\"id\": \"589\",\n" +
            "\"name\": \"Bunga Kusuma\",\n" +
            "\"description\": \"Mengambil pembayaran kredit Bank XYZ\",\n" +
            "\"address\": \"Jalan Mangga 15\",\n" +
            "\"region\": \"Jakarta Barat\",\n" +
            "\"deadline\": \"2017-04-22 10:00:00\",\n" +
            "\"task_status\": \"success\",\n" +
            "\"latitude\": \"-6.17527664\",\n" +
            "\"longitude\": \"106.77625615\",\n" +
            "\"courier_id\": \"666\",\n" +
            "\"assigned_by\": \"Lutfi Wiguna\",\n" +
            "\"category\": \"0\"\n" +
            "}, {\n" +
            "\"id\": \"590\",\n" +
            "\"name\": \"Lukman Fardi\",\n" +
            "\"description\": \"Mengambil aplikasi kredit Bank CCC\",\n" +
            "\"address\": \"Jalan Cendrawasih II\",\n" +
            "\"region\": \"Jakarta Selatan\",\n" +
            "\"deadline\": \"2017-04-22 12:00:00\",\n" +
            "\"task_status\": \"not done\",\n" +
            "\"latitude\": \"-6.26813472\",\n" +
            "\"longitude\": \"106.79631069\",\n" +
            "\"courier_id\": \"666\",\n" +
            "\"assigned_by\": \"Lutfi Wiguna\",\n" +
            "\"category\": \"0\"\n" +
            "}, {\n" +
            "\"id\": \"591\",\n" +
            "\"name\": \"Siti Inayah\",\n" +
            "\"description\": \"Mengantar dokumen KTA Bank DDD\",\n" +
            "\"address\": \"Masjid Istiqlal\",\n" +
            "\"region\": \"Jakarta Pusat\",\n" +
            "\"deadline\": \"2017-04-22 14:00:00\",\n" +
            "\"task_status\": \"not done\",\n" +
            "\"latitude\": \"-6.1699936\",\n" +
            "\"longitude\": \"106.8309278\",\n" +
            "\"courier_id\": \"666\",\n" +
            "\"assigned_by\": \"Lutfi Wiguna\",\n" +
            "\"category\": \"1\"\n" +
            "}, {\n" +
            "\"id\": \"592\",\n" +
            "\"name\": \"Daryl Ashlake\",\n" +
            "\"description\": \"Mengambil dokumen kredit motor\",\n" +
            "\"address\": \"Kebun Binatang Ragunan\",\n" +
            "\"region\": \"Jakarta Selatan\",\n" +
            "\"deadline\": \"2017-04-22 16:00:00\",\n" +
            "\"task_status\": \"not done\",\n" +
            "\"latitude\": \"-6.312426\",\n" +
            "\"longitude\": \"106.821989\",\n" +
            "\"courier_id\": \"666\",\n" +
            "\"assigned_by\": \"Lutfi Wiguna\",\n" +
            "\"category\": \"2\"\n" +
            "}]\n" +
            "}";

    @Before
    public void prepare() {
        listTaskPresenter = new ListTaskPresenter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Gson gson = gsonBuilder.create();
        expectedTaskResponseRecord = gson.fromJson(MOCK_JSON, TaskResponseRecord.class);
    }

    @Test
    public void createTasks() {
        listTaskPresenter.createTasks();
        TaskResponseRecord actualTaskResponseRecord = listTaskPresenter.getTasks();

        assertEquals(expectedTaskResponseRecord, actualTaskResponseRecord);

        expectedTaskResponseRecord.tasks.remove(0);
        assertNotEquals(expectedTaskResponseRecord, actualTaskResponseRecord);
    }

    @Test
    public void getCurrentTask() {
        TaskRecord actualTask = listTaskPresenter.getCurrentTask();
        TaskRecord expectedTask = expectedTaskResponseRecord.tasks.get(2);
        assertEquals( expectedTask, actualTask);
    }
}