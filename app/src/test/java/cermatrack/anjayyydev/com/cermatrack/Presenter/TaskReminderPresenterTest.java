package cermatrack.anjayyydev.com.cermatrack.Presenter;

import android.app.Activity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by muhammadgilangjanuar on 4/7/17.
 */
public class TaskReminderPresenterTest {
    TaskReminderPresenter taskReminderPresenter;
    ListTaskPresenter listTaskPresenter;

    @Before
    public void prepare() {
        listTaskPresenter = new ListTaskPresenter();
        listTaskPresenter.createTasks();

        taskReminderPresenter = new TaskReminderPresenter(new Activity());
    }

    @Test
    public void createNotif() {
        try {
            taskReminderPresenter.createNotif();
            assertEquals(listTaskPresenter.getTasks(), taskReminderPresenter.taskResponses);

            assertNotEquals(null, taskReminderPresenter.taskResponses);
        } catch (Exception e) {

        }
    }

}