package cermatrack.anjayyydev.com.cermatrack.Presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import cermatrack.anjayyydev.com.cermatrack.Model.ResponseRecord;
import cermatrack.anjayyydev.com.cermatrack.Model.TaskRecord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by muhammadgilangjanuar on 4/13/17.
 */
public class TripPresenterTest {

    TripPresenter tripPresenter;
    ResponseRecord expectedResponseRecord;
    TaskRecord taskRecord;
    static final String MOCK_JSON = "{\n" +
            "    \"status\": 200\n" +
            "}";

    @Before
    public void before() {
        tripPresenter = new TripPresenter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Gson gson = gsonBuilder.create();
        expectedResponseRecord = gson.fromJson(MOCK_JSON, ResponseRecord.class);

        ListTaskPresenter listTaskPresenter = new ListTaskPresenter();
        listTaskPresenter.createTasks();
        taskRecord = listTaskPresenter.getTasks().tasks.get(0);
    }

    @Test
    public void start() {
        // reset respose
        this.tripPresenter.response = new ResponseRecord();

        this.tripPresenter.start(taskRecord);
        assertEquals(expectedResponseRecord, this.tripPresenter.response);

        String mockJsonFalse = "{\n" +
                "    \"status\": 302\n" +
                "}";
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Gson gson = gsonBuilder.create();
        ResponseRecord expectedFalseResponseRecord = gson.fromJson(mockJsonFalse, ResponseRecord.class);

        assertNotEquals(expectedFalseResponseRecord, this.tripPresenter.response);
    }

    @Test
    public void cancel() {
        // reset respose
        this.tripPresenter.response = new ResponseRecord();

        String reason = "Ada keperluan lain";
        this.tripPresenter.cancel(taskRecord, reason);
        assertEquals(expectedResponseRecord, this.tripPresenter.response);

        String mockJsonFalse = "{\n" +
                "    \"status\": 302\n" +
                "}";
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Gson gson = gsonBuilder.create();
        ResponseRecord expectedFalseResponseRecord = gson.fromJson(mockJsonFalse, ResponseRecord.class);
        assertNotEquals(expectedFalseResponseRecord, this.tripPresenter.response);
    }

    @Test
    public void cancel_null_reason() {
        this.tripPresenter.response = new ResponseRecord();
        String goodReason = "Ada keperluan lain";
        this.tripPresenter.cancel(taskRecord, goodReason);

        TripPresenter badPresenter = new TripPresenter();
        String badReason = null;
        badPresenter.cancel(taskRecord, badReason);
        assertNotEquals(badPresenter, this.tripPresenter);
    }

    @Test
    public void cancel_no_reason() {
        this.tripPresenter.response = new ResponseRecord();
        String goodReason = "Ada keperluan lain";
        this.tripPresenter.cancel(taskRecord, goodReason);

        TripPresenter badPresenter = new TripPresenter();
        String badReason = "";
        badPresenter.cancel(taskRecord, badReason);
        assertNotEquals(badPresenter, this.tripPresenter);
    }

}